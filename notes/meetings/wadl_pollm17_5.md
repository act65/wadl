# Initial Meeting

**Attendees:**
- Alex Telfar
- Winston Poh
- Luis Slyfield
- Demelza Robinson

![Example Image](../images/akshually.png)

**What are people’s goals and interests for this project:**
- Top trump cards - evasiveness
- How individuals interact with politician’s social media
- Distilling parliamentary discourse
- Switzerland pros and cons report
- Convenience for users - meaningful 
- Misinformation, misleading political speech
- Flag what parts of speech potentially 
- Plug-in
- Transition of policies

**Overall Goal:**
- Lots of data - scrape some websites
- LLMs and politics

**Patterns of speech that politicians use to not say much:**
- Applause light, “we should try and balance the opportunities and risks of AI”
- Say the opposite, does it make sense, to determine worth debating?
- Passive voice

**Bell?ngcat**

**Newsroom**

[pol.is](https://pol.is/)

**Aside:**
[Hipkins vs Luxon debate](https://www.rnz.co.nz/news/political/498911/hipkins-vs-luxon-the-one-word-christopher-luxon-said-76-times-in-newshub-s-debate)

## Goal

I wanted to test whether an LLM (Claude here) can feasibly identify techniques of discourse politicians use to avoid answering questions, in a parliament setting.

## Task

Asked Claude to come up with a list of such methods, then identify examples of this from a Hansard excerpt (attached below):

![Example Image](../images/claude_evasiveparliament.png)

[Hansard Excerpt](https://www.parliament.nz/en/pb/hansard-debates/rhr/combined/HansDeb_20240509_20240509_20)

**Yes, I can identify several rhetorical techniques and strategies employed by politicians in this transcript to dodge or avoid directly answering questions: questionable vs reasonable?**

### Examples

1. **Pivoting/Changing the subject:**
   - When asked about funding for dry-year risk programs, the Minister for Energy pivoted to criticizing the previous government's policies instead of directly answering where the funding went.
   - Yes, I think this is a reasonable critique; he indeed often does over-reference the previous governments’ shortcomings.

2. **Filibustering/Excessive details:**
   - The Minister for Energy provided excessive details and figures when asked about energy security reports, rather than directly answering the question.
   - I don’t think this is reasonable; MPs often have fact sheets in front of them that they read from?? Claude maybe misunderstands the importance of the setting here.

3. **Attacking the question:**
   - When asked if economists support his tax cuts, the Minister of Finance attacked the premise of the question by saying economists couldn't have commented since the full tax package hasn't been delivered yet.
   - Questionable also?

4. **Claiming ignorance:**
   - When asked about advice received regarding the fast-track consent process, the Minister of Climate Change claimed some aspects were not within his delegation.
   - Hmmm :|

5. **Promising a follow-up:**
   - When asked to guarantee details about the Nelson Hospital rebuild, the Minister of Health said the project was still in the design phase towards a business case, implying details would come later.
   - How is this a problem?

6. **Applause lines:**
   - The Minister of Local Government encouraged people in Tauranga to vote or stand for election when announcing the restoration of democracy there, which could generate applause from supporters.

So several techniques were used by ministers to avoid directly addressing aspects of questions posed by opposition parties.




**Ministers and their portfolios:**
- Hon Nicola Willis - Minister of Finance
- Hon Louise Upston - Minister for Social Development and Employment
- Hon Simeon Brown - Minister for Energy, Minister of Local Government
- Hon Casey Costello - Associate Minister of Health
- Hon Todd McClay - Minister for Trade
- Hon Penny Simmonds - Minister for Tertiary Education and Skills
- Hon Tama Potaka - Minister of Conservation (answering on behalf of the Minister responsible for RMA Reform)
- Hon Simon Watts - Minister of Climate Change
- Hon Dr. Shane Reti - Minister of Health
