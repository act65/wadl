So if I had had to list your goals;

1. Teach people about cognitive biases, and rational / critical thinking, that you cannot trust people on their word, ...
2. Distill political (hot) topics to make them more digestible (to help with the limited information we can process) so people can engage with them
3. Expose people to different perspectives (to encourage empathy and understanding)

***

The funny thing is, from how you describe the game, it simply seems like a way for people to talk to others who have different opinions from their own.

My question is; why would anyone engage with this game / content (who doesn’t already think rational thinking / empathy / … are good things)?
So, how can we make it 'fun'?

1. A game to expose others? (I imagine it’s easier for us to identify cognitive biases in others - where the 'others' might be NPCs powered by LLMs? You get points when you identify a bias in an NPC)
2. ??? (no ideas yet)
3. Acting is a fun thing to do. We can reframe learning to empathise as, how good are you are pretending to be someone else? Or how well do you know the opposition's arguments? (like a GAN? have a critic that tries to identify the true believer vs the actor)