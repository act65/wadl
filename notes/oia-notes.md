Goals;

- transparency! get access to anything / everything the govt has.


Questions

- Which Govt agencies are supported? Which are not?
- 



# Tools

> Send your request to the right place or person

You should make your request directly to the agency, local authority, Minister or other government entity you believe to hold the specific information you want, or to be most closely related to the topic of your request. 

Ok. So, how do I know which agency to contact?
Would be good to have a tool that can be given a question and it can suggest the agency / person to contact.

(will minimise delays)

***

generate all requests based on a question.
the phrasing might be important?


# Costs

> Usually there will be no charge for the Department’s response to an OIA request, however if there is a substantial cost to the Department for providing the information, you may be asked to meet some or all of the cost of your request.

Hrmm. That doesn't seem very 'open'. !?