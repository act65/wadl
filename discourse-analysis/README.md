Want a tool that can highlight / extract parts of speech.

For example;

- we may care about the 'evasiveness' of a politicians speech.
- we may want to identify any fallacious arguments in a speech.

Note. It would be good to construct some test sets for each of these tasks.
So we can evaluate the performance of the tools.

resources

- https://en.wikipedia.org/wiki/List_of_fallacies
- 