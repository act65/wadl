Impersonation game idea.

How good are you are pretending to be someone else? 
How well do you know their arguments? 

(to enourage us to;
 - see the other side of the argument, 
 - emapthise with those different from us, 
 - engage with different perspectives)

The game setting is;

We use NZ politicians as the targets for impersonation, one is chosen.
You are being interviewed by a journalist.
You are asked questions and prompted for your opinions on current events.
You have to respond as if you were the chosen politician. 

You get points for how well your answers / opinions match the chosen politician's.

***

Implementation details;

- We can scrape the internet for interviews, speeches, ... of the chosen politician.
- We can 'train' a critic by showing it examples of the ground truth.
- use a text interface (say on a website).

***

Issues / todos / sticking points.

- this has potential to be insulting / offensive to the politicians? (especially if our critic rewards incorrect answers / bad behaviour)
- would this really be 'fun'?
- can we use a LLM as the critic? how much training would we need?
- where are the interview questions coming from? use current events? (like an advanced version of the stuff quiz?)
- the critic might focus too much on speech patterns / word choice rather than the content of the answers? (how to avoid this?)

***

Extensions;

- could use a leaderboard? (use gemoetric mean of the scores to rank players?)
- what about other politically important people?
- assuming it works, extend to other countries?
- imbue the journalist with a keen ear for cognitive biases?